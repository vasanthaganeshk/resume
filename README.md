# My Resume

+ This is a clone of [Deedy-Resume](https://github.com/deedy/Deedy-Resume)
+ run `xelatex vasa_resume.tex` to generate the pdf
+ run `convert -flatten vasa_resume.pdf vasa_resume.png` to generate png (you need ImageMagick for this)

## Preview
![alt text](https://gitlab.com/vasanthaganeshk/resume/raw/master/vasa_resume.png "Resume Preview")
